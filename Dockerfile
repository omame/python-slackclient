FROM python:3.8-alpine as build

RUN apk add --no-cache build-base findutils && \
    pip install slackclient && \
    pip uninstall -y pip && \
    rm -rf /usr/src/python /root/.cache /root/.ash_history /root/.pip_history && \
	find /usr/local -depth \
		\( \
			\( -type d -a \( -name test -o -name tests -o -name idle_test \) \) \
			-o \( -type f -a \( -name '*.pyc' -o -name '*.pyo' -o -name '*.a' \) \) \
		\) | xargs rm -rf && \
    apk del build-base findutils

FROM scratch
COPY --from=build / /
